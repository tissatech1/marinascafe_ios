//
//  OtherHome.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 5/3/21.
//

import UIKit
import SideMenu
import ProgressHUD
import SafariServices
import WebKit

class OtherHome: UIViewController, WKNavigationDelegate {
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        webView.navigationDelegate = self
        webView.backgroundColor = .clear
    let url = URL(string: "https://ordering.chownow.com/order/28861/locations")!
      //  let url = URL(string: "https://ordersave.com/restaurant/marinascafe")!
    webView.load(URLRequest(url: url))
    webView.allowsBackForwardNavigationGestures = true
        webView.scrollView.isScrollEnabled = true
       self.webView.addObserver(self, forKeyPath: #keyPath(WKWebView.isLoading), options: .new, context: nil)

    }
    
    @IBAction func reload(_ sender: UIButton) {

      //  webView.reload()
        
        if(self.webView.canGoBack) {
                self.webView.goBack()
               }

    }
    
    @IBAction func moreClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
            if keyPath == "loading" {
                if webView.isLoading {
                    ProgressHUD.show()
                    
                } else {
                    
                    ProgressHUD.dismiss()
                }
            }
        }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("End loading")
//        webView.evaluateJavaScript("document.getElementsByTagName(div).remove();", completionHandler: { (response, error) -> Void in
//                           })
        ProgressHUD.dismiss()
    }

        func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
            ProgressHUD.show()
           // print(webView.url!)
        }

        func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {

            print("error  - \(error)")
        }


}
